/*  Matthew Scheirer (C) 2014

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.1
import QtMultimedia 5.0

ApplicationWindow {
    title: qsTr("Music Player")
    id: window

    property int buttonEdge : 48

    minimumWidth: 400
    minimumHeight: 240

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            MenuItem {
                text: qsTr("&Open")
                iconName: "document-open"
                shortcut: StandardKey.Open
                onTriggered: openAudio.open()
            }

            MenuItem {
                text: qsTr("&Exit")
                iconName: "application-exit"
                shortcut: StandardKey.Quit
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("&Help")
            MenuItem {
                text: qsTr("&About")
                iconName: "help-about"
                onTriggered: about.open()
            }
        }
    }

    TableView {
        id: playlistView
        //anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: trackTime.top
        anchors.bottomMargin: 5

        property int columnWidth: window.width / 6 - 1

        model: playlist

        sortIndicatorVisible: true
        selectionMode: SelectionMode.ExtendedSelection

        TableViewColumn { role: "track"; title: "Track"; width: playlistView.columnWidth }
        TableViewColumn { role: "title"; title: "Title"; width: playlistView.columnWidth }
        TableViewColumn { role: "album"; title: "Album"; width: playlistView.columnWidth }
        TableViewColumn { role: "artist"; title: "Artist"; width: playlistView.columnWidth }
        TableViewColumn { role: "genre"; title: "Genre"; width: playlistView.columnWidth }
        TableViewColumn { role: "length"; title: "Length"; width: playlistView.columnWidth }

        onSortIndicatorColumnChanged: model.sort(getColumn(sortIndicatorColumn).role, sortIndicatorOrder)
        onSortIndicatorOrderChanged: model.sort(getColumn(sortIndicatorColumn).role, sortIndicatorOrder)

        onDoubleClicked: playlist.play(row)

        //rowDelegate: Item {}
    }

    Slider {
        id: trackTime
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: controls.left
        anchors.rightMargin: 10
        anchors.verticalCenter: controls.verticalCenter
        anchors.bottom: statusBar.top
        stepSize: 1
        minimumValue: 0
        maximumValue: nowPlaying.duration
        updateValueWhileDragging: false
        // This will generate assignment errors whenever you press the slider, but there is no other
        // easy way to only update the value when you aren't manipulating the track position.
        value: if(!pressed) nowPlaying.position
        onPressedChanged: if(!pressed && nowPlaying.seekable) nowPlaying.seek(value)
    }

    Row {
        id: controls
        anchors.bottom: statusBar.top
        anchors.right: parent.right
        Button { action: playPauseTrack }
        Button { action: stopTrack }
    }

    StatusBar {
        id: statusBar
        anchors.bottom: parent.bottom
        Label {
            id: trackTitle
            anchors.left: parent.left
            width: parent.width - trackDuration.width
            text: nowPlaying.seekable ? nowPlaying.metaData.title : ""
            elide: Text.ElideMiddle
        }
        Label {
            id: trackDuration
            text: nowPlaying.seekable ? fixedWidthMins(nowPlaying.position) + " / " + fixedWidthMins(nowPlaying.duration) : ""
            anchors.right: parent.right
        }
    }

    FileDialog {
        id: openAudio
        title: "Select a Music Track"
        nameFilters: ["Audio Files(*.oga *.ogg *.flac *.opus *.mp3 *.m4a *.aac)"]
        selectMultiple: true
        onAccepted: populatePlaylist(fileUrls)
    }

    MessageDialog {
        id: about
        title: "About"
        text: qsTr("Matthew Scheirer (C) 2014.
An attempt to explore the power of QtQuick 2.2, by writing a comprehensive music player in minimal lines of code.")
        informativeText: qsTr("Licensed under the GNU GPL3 or later.")
        detailedText: qsTr("This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public \
License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY \
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.")
        standardButtons: StandardButton.Close
    }

    Audio {
        id: nowPlaying
        autoPlay: true
        onStatusChanged: if(status === MediaPlayer.EndOfMedia) playlist.playNext()
    }

    Audio {
        id: dataParser

        property bool parsing
        property var parseQueue: []

        onSourceChanged: if(!parsing) parsing = true

        onStatusChanged: {
            if(status === MediaPlayer.Loaded && parsing) {
                // When we have a library, this information should enter that, and be pulled from it into the playlist if necessary.
                // WARNING: We shift here because if we try to use source, that is a url type - despite documentation considering it a "basic" type,
                // adding it to a ListMdoel treats it like an object and you won't get a URL back by indexing it out.
                playlist.append({"url": parseQueue.shift(), "track": metaData.trackNumber, "title": metaData.title, "album": metaData.albumTitle,
                                 "artist": metaData.albumArtist, "genre": metaData.genre, "length": msToMins(duration)})
                // If this were multithreaded this would be a race condition. If necessary, make it write lock on source.
                parsing = false
                if(playlist.count === 1 && nowPlaying.status === MediaPlayer.NoMedia) playlist.play(0)
                if(parseQueue.length > 0) source = parseQueue[0]
            }
        }

        function push(url) {
            parseQueue.push(url)
            if(!parsing) source = parseQueue[0]
        }
    }

    ListModel {
        id: playlist

        property int playingIndex
        //TODO: This should be a preserved property, but defaults false when we implement that.
        property bool repeat: true
        property bool autoplay: true

        /* This complex function is necessary to make sure we keep our selections in the tableview accurate when we move elements. The more
                   appropriate answer is probably to override that functionality in the row to highlight the rows - there is some functionailty already passed
                   to the row delegate regarding this */
        function checkSelection(from, to) {
            if(playlistView.selection.count === 0) return
            var fromSelected = playlistView.selection.contains(from)
            var toSelected = playlistView.selection.contains(to)
            console.log(fromSelected + ' ' + toSelected)
            if(fromSelected && !toSelected) {
                playlistView.selection.deselect(from, 1)
                playlistView.selection.select(to, 1)
            } else if(!fromSelected && toSelected) {
                playlistView.selection.deselect(to, 1)
                playlistView.selection.select(from, 1)
            }
        }

        /* Insertion sort. Used because quicksort isn't stable, and mergesort would require an out of place sort meaning cloning QML objects
                   which while perfectly doable (Qt.createComponent & for(var in obj) newobj = oldobj, recursively for all non-simple types) it would
                   be absurdly painful to implement and debug.

                    TODO: Intersting bug, if track titles have numbers (ie, HR116 and HR73) the HR116 is sorted before HR73 because the string compare ends there.
                    It would be *really* time consuming to reimplement strcmp to compare number slots identically and to ignore whitespace, though that is probably
                    expected behavior in this circumstance.*/
        function sort(role, order) {
            var comp = (order === Qt.AscendingOrder) ? function(x, y) { return x > y } : function(x, y) { return x < y }
            for(var index = 0; index < count; index++) {
                for(var key = get(index)[role], counter = index, adjacent = index - 1;
                    counter > 0 && comp(get(adjacent)[role], key);
                    counter--, adjacent--) {
                    console.log(adjacent + ' ' + counter)
                    checkSelection(adjacent, counter)
                    if(adjacent === playingIndex) playingIndex++
                    if(counter === playingIndex) playingIndex--
                    move(adjacent, counter, 1)
                }
            }
        }

        function playNext() {
            if(playingIndex === count - 1 && repeat) play(0)
            else play(playingIndex + 1)
        }

        function play(index) {
            playingIndex = index
            nowPlaying.source = get(index).url
        }
    }

    Action {
        id: playPauseTrack
        property bool playing: nowPlaying.playbackState === Audio.PlayingState
        text: playing ? qsTr("&Pause") : qsTr("&Play")
        iconName: playing ? "media-playback-pause" : "media-playback-start"
        iconSource: playing ? "icons/kfaenza_media-pause.png" : "icons/kfaenza_media-play.png"
        shortcut: "space"
        tooltip: playing ? qsTr("Pause Track") : qsTr("Play Track")
        onTriggered: playing ? nowPlaying.pause() : nowPlaying.play()
    }

    Action {
        id: stopTrack
        text: qsTr("&Stop")
        iconName: "media-playback-stop"
        iconSource: "icons/kfaenza_media-stop.png"
        tooltip: qsTr("Stop Track")
        onTriggered: nowPlaying.stop()
    }

    function populatePlaylist(urls) {
        // When we start persistently storing track / playlist / library info, we should check if a track is already in the library before parsing.
        for(var index in urls) dataParser.push(urls[index])
        //if(!dataParser.parsing) dataParser.source = dataParser.parseQueue.shift()
    }

    function msToMins(ms) {
        return Math.floor(ms / 60000) + ':' + Math.floor(ms / 1000 % 60)
    }

    function fixedWidthMins(ms) {
        var minutes = Math.floor(ms / 60000)
        var seconds = Math.floor(ms / 1000 % 60)

        minutes = minutes < 10 ? '0' + minutes : minutes
        seconds = seconds < 10 ? '0' + seconds : seconds

        return minutes + ':' + seconds
    }
}
